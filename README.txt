Tugas 3 Pemrograman Integratif

oleh : 	Olivia - 18211014
		Ardian Indra Gunawan - 18211029
		
Untuk akses via localhost :
1. Eksekusi script "download-progin-wondows.bat" lewat Git Bash pada Windows
2. Masukan "index.php" pada direktori yang sama
3. Buka "index.php" lewat browser

Untuk akses via server :
1. Eksekusi file index.php
2. Pilih menu "Akses Tugas Teman (Web)"
